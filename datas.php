<?php
require_once('config.php');

//trabalhando com datas
//string date(string $format[,int $timestamp = time()]);
echo date('d-m-Y').'<br>';
echo date('H:i:s').'<br>';
echo date_default_timezone_get().'<br>';
echo date('D w l d-m-y').'<br>';
echo date(DATE_RSS).'<br>';

$timestamp = time();
//1568934965
echo $timestamp.'<br>';
echo date('D, d M Y H:i:s',$timestamp).'<br>';

$timestamp = mktime(00,30,00,03,16,1965);
echo $timestamp.'<br>';
echo date('d-m-Y H:i:s',$timestamp ).'<br>';
//recebe data e hota do usuário
$data_digitada = '09-10-1998 08:00:05';
//converte data e hora recebida em timestamp
$timestamp1 = strtotime($data_digitada);
echo $timestamp1.'<br>';
echo date('d/m/Y H:i:s', $timestamp1).'<br>';

$timestamp2 = strtotime('+235 minute',$timestamp1);
echo $timestamp2.'<br>';
echo date('d/m/Y H:i:s', $timestamp2).'<br>';

$data_padrao = new DateTime();
//print_r($data_padrao);

$data_fin = new DateTime('17-09-2019 15:32:09');
$dia_amanha = new DateTime('+1 day');

echo $data_fin->format('d m Y');



?>