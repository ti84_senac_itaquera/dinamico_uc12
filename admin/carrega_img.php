<?php
require_once('conexao.php');
// verificar se o usuário clicou no botão cadastrar 
if (isset($_POST['cadastro'])) {
    //recuperar dados dos campos do form
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $foto = $_FILES['foto'];

   // var_dump($foto);

    if(!empty($foto['name'])){
        //largura máxima em pixels
        $largura = 640;
        //altura máxima em pixels
        $altura = 425;
        //tamanho máximo em bytes
        $tamanho = 300000;
        
        $error = array();
        //verifica se é uma imagem
        if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/",$foto['type'])){
            $error[1] = "Isso não é uma imagem!";
        }
        // recuperando as dimensões
        $dimensoes = getimagesize($foto['tmp_name']);
        //var_dump($dimensoes);
        //verifica se a largura da imagem é maior que a permitida
        if($dimensoes[0]>$largura){
            $error[2] = "A largura da imagem é maior que a permitida! Não deve ultrapassar ".$largura." pixels.";
        }
        //verifica se a altura da imagem é maior que a permitida
        if($dimensoes[1]>$altura){
            $error[3] = "A altura da imagem é maior que a permitida!  Não deve ultrapassar ".$altura." pixels.";
        }
        //verifica se o tamanho da imagem é maior que a permitida
        if ($foto['size']> $tamanho){
            $error[4] = "O tamanho da imagem é maior que o permitido! Não deve ultrapassar ".$tamanho." bytes." ;
        }
        //se não houver erros
        if (count($error)==0){
            //recupera a extensão do arquivo
            preg_match("/\.(gif|bmp|png|jpg){1}$/i",$foto["name"],$ext);
            // gera um nome de arquivo único para a imagem
            $nome_imagem = md5(uniqid(time())).".".$ext[1];
            echo "<br>";
            //caminho para armazenar as imagens
            $caminho_imagem = "foto/".$nome_imagem;
            // realiza o upload da imagem a partir do espaço temporário
            move_uploaded_file($foto['tmp_name'],$caminho_imagem);
            //insere os dados dos usuário 
            $cmd = $conn->prepare("insert into usuario (nome, email, foto) values(:nome, :email, :foto)");
            $result = $cmd->execute(array(
                ":nome"=>$nome,
                ":email"=>$email,
                ":foto"=>$nome_imagem
            )); 
            if($result==true){            
                echo "<br> Usuário inserido com sucesso.";
            }

        }
    }
    if(count($error)!=0){
        foreach ($error as $erro) {
            echo $erro."<br>";
        }
    }

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadastro de Usuário</title>

</head>
<body>
    <!-- <h1>Novo Usuário</h1> -->
    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data"  name="cadastro_form" >
        Nome:<br>
        <input type="text" name="nome" class="form-control"><br><br>
        Email:<br>
        <input type="text" name="email" class="form-control"><br><br>
        Foto de exibição:<br>
        <input type="file" name="foto" class="form-control">
        <br><br>
        <input type="submit" name="cadastro" value="Cadastrar" class="btn btn-success">
    </form>
    <br>
    <hr>
    <h2>Usuários Cadastrados</h2>
    <?php
        $cmd = $conn->prepare("select * from usuario");
        $cmd->execute();
        $result = $cmd->fetchAll(PDO::FETCH_ASSOC);
        //var_dump($result);
        foreach($result as $usuario){
            echo "<img src='foto/".$usuario['foto']."' width='56px' height='48'>";
            echo "<br>";
            echo "Nome: ".$usuario['nome'];
            echo "<br>";
            echo "Email: ".$usuario['email'];
            echo "<br>";
        }
    ?>

</body>
</html>